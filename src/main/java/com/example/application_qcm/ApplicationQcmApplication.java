package com.example.application_qcm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationQcmApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApplicationQcmApplication.class, args);
	}

}
