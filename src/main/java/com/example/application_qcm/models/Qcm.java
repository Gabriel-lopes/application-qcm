package com.example.application_qcm.models;

import java.util.Objects;

public class Qcm {
    private Integer id;
    private String nom;

    public Qcm() {
    }

    public Qcm(Integer id, String nom) {
        this.id = id;
        this.nom = nom;
    }
    
    public Qcm(String nom) {
        this.nom = nom;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Qcm id(Integer id) {
        setId(id);
        return this;
    }

    public Qcm nom(String nom) {
        setNom(nom);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Qcm)) {
            return false;
        }
        Qcm qcm = (Qcm) o;
        return Objects.equals(id, qcm.id) && Objects.equals(nom, qcm.nom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom);
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", nom='" + getNom() + "'" +
            "}";
    }

}
