package com.example.application_qcm.models;

import java.util.Objects;

public class Utilisateur {
    private Integer Id;
    private String nom, email, prenom;

    public Utilisateur() {
    }

    public Utilisateur(Integer Id, String nom, String email, String prenom) {
        this.Id = Id;
        this.nom = nom;
        this.email = email;
        this.prenom = prenom;
    }

    public Utilisateur(String nom, String email, String prenom) {
        this.nom = nom;
        this.email = email;
        this.prenom = prenom;
    }

    public Integer getId() {
        return this.Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Utilisateur Id(Integer Id) {
        setId(Id);
        return this;
    }

    public Utilisateur nom(String nom) {
        setNom(nom);
        return this;
    }

    public Utilisateur email(String email) {
        setEmail(email);
        return this;
    }

    public Utilisateur prenom(String prenom) {
        setPrenom(prenom);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Utilisateur)) {
            return false;
        }
        Utilisateur utilisateur = (Utilisateur) o;
        return Objects.equals(Id, utilisateur.Id) && Objects.equals(nom, utilisateur.nom) && Objects.equals(email, utilisateur.email) && Objects.equals(prenom, utilisateur.prenom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Id, nom, email, prenom);
    }

    @Override
    public String toString() {
        return "{" +
            " Id='" + getId() + "'" +
            ", nom='" + getNom() + "'" +
            ", email='" + getEmail() + "'" +
            ", prenom='" + getPrenom() + "'" +
            "}";
    }


}
