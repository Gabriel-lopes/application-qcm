package com.example.application_qcm.models;

import java.util.Objects;

public class Reponse {
    private Integer id, idQuestion;
    private String reponse;
    private Boolean veracite;

    public Reponse() {
    }

    public Reponse(Integer id, Integer idQuestion, String reponse, Boolean veracite) {
        this.id = id;
        this.idQuestion = idQuestion;
        this.reponse = reponse;
        this.veracite = veracite;
    }
    public Reponse(Integer idQuestion, String reponse, Boolean veracite) {

        this.idQuestion = idQuestion;
        this.reponse = reponse;
        this.veracite = veracite;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdQuestion() {
        return this.idQuestion;
    }

    public void setIdQuestion(Integer idQuestion) {
        this.idQuestion = idQuestion;
    }

    public String getReponse() {
        return this.reponse;
    }

    public void setReponse(String reponse) {
        this.reponse = reponse;
    }

    public Boolean isVeracite() {
        return this.veracite;
    }

    public Boolean getVeracite() {
        return this.veracite;
    }

    public void setVeracite(Boolean veracite) {
        this.veracite = veracite;
    }

    public Reponse id(Integer id) {
        setId(id);
        return this;
    }

    public Reponse idQuestion(Integer idQuestion) {
        setIdQuestion(idQuestion);
        return this;
    }

    public Reponse reponse(String reponse) {
        setReponse(reponse);
        return this;
    }

    public Reponse veracite(Boolean veracite) {
        setVeracite(veracite);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Reponse)) {
            return false;
        }
        Reponse reponse = (Reponse) o;
        return Objects.equals(id, reponse.id) && Objects.equals(idQuestion, reponse.idQuestion) && Objects.equals(reponse, reponse.reponse) && Objects.equals(veracite, reponse.veracite);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, idQuestion, reponse, veracite);
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", idQuestion='" + getIdQuestion() + "'" +
            ", reponse='" + getReponse() + "'" +
            ", veracite='" + isVeracite() + "'" +
            "}";
    }

}
