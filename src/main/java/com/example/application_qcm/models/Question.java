package com.example.application_qcm.models;

import java.util.Objects;

public class Question {
    private Integer id, idQcm;
    private String question;


    public Question() {
    }

    public Question(Integer id, Integer idQcm, String question) {
        this.id = id;
        this.idQcm = idQcm;
        this.question = question;
    }
    public Question(Integer idQcm, String question) {
        this.idQcm = idQcm;
        this.question = question;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdQcm() {
        return this.idQcm;
    }

    public void setIdQcm(Integer idQcm) {
        this.idQcm = idQcm;
    }

    public String getQuestion() {
        return this.question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Question id(Integer id) {
        setId(id);
        return this;
    }

    public Question idQcm(Integer idQcm) {
        setIdQcm(idQcm);
        return this;
    }

    public Question question(String question) {
        setQuestion(question);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Question)) {
            return false;
        }
        Question questions = (Question) o;
        return Objects.equals(id, questions.id) && Objects.equals(idQcm, questions.idQcm) && Objects.equals(question, questions.question);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, idQcm, question);
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", idQcm='" + getIdQcm() + "'" +
            ", question='" + getQuestion() + "'" +
            "}";
    }

}
