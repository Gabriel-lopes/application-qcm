package com.example.application_qcm.models;

import java.util.Objects;

public class Score {
    private Integer idQcm, idUtilisateur, score;

    public Score() {
    }

    public Score(Integer idQcm, Integer idUtilisateur, Integer score) {
        this.idQcm = idQcm;
        this.idUtilisateur = idUtilisateur;
        this.score = score;
    }

    public Score(Integer score) {
        this.score = score;
    }

    public Integer getIdQcm() {
        return this.idQcm;
    }

    public void setIdQcm(Integer idQcm) {
        this.idQcm = idQcm;
    }

    public Integer getIdUtilisateur() {
        return this.idUtilisateur;
    }

    public void setIdUtilisateur(Integer idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public Integer getScore() {
        return this.score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Score idQcm(Integer idQcm) {
        setIdQcm(idQcm);
        return this;
    }

    public Score idUtilisateur(Integer idUtilisateur) {
        setIdUtilisateur(idUtilisateur);
        return this;
    }

    public Score score(Integer score) {
        setScore(score);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Score)) {
            return false;
        }
        Score score = (Score) o;
        return Objects.equals(idQcm, score.idQcm) && Objects.equals(idUtilisateur, score.idUtilisateur) && Objects.equals(score, score.score);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idQcm, idUtilisateur, score);
    }

    @Override
    public String toString() {
        return "{" +
            " idQcm='" + getIdQcm() + "'" +
            ", idUtilisateur='" + getIdUtilisateur() + "'" +
            ", score='" + getScore() + "'" +
            "}";
    }

}
