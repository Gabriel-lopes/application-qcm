package com.example.application_qcm.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.application_qcm.models.Reponse;
import com.example.application_qcm.repositories.ReponseRepository;

@RestController
@RequestMapping("api")
public class ReponseController {
    @Autowired
    private ReponseRepository repository;

    @GetMapping("/reponses")
    public List<Reponse> index(){
        return repository.findAll();
    }

    @GetMapping("/reponses/{id}")
    public Reponse show(@PathVariable String id){
        int questionId = Integer.parseInt(id);
        return repository.findById(questionId).get();
    }

    @PostMapping("/reponses/search")
    public List<Reponse> search(@RequestBody Map<String, String> body){
        String searchTerm = body.get("text");
        return repository.findByFirstnameContaining(searchTerm);
    }
    @PutMapping("/reponse/{id}")
    public Reponse update(@PathVariable String id, @RequestBody Map<String, String> body){
        int reponseId = Integer.parseInt(id);
        // getting blog
        Reponse reponse = repository.findById(reponseId).get();
        reponse.setReponse(body.get("reponse"));
        return repository.save(reponse);
    }

    @DeleteMapping("reponse/{id}")
    public boolean delete(@PathVariable String id){
        int questionId = Integer.parseInt(id);
        repository.deleteById(questionId);
        return true;
    }
}
