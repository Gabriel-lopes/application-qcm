package com.example.application_qcm.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.application_qcm.models.Utilisateur;
import com.example.application_qcm.repositories.UtilisateurRepository;

@RestController
@RequestMapping("api")
public class UtilisateurController {
    @Autowired
    private UtilisateurRepository repository;

    @GetMapping("/utilisateurs")
    public List<Utilisateur> index(){
        return repository.findAll();
    }

    @GetMapping("/utilisateurs/{id}")
    public Utilisateur show(@PathVariable String id){
        int UtilisateurId = Integer.parseInt(id);
        return repository.findById(UtilisateurId).get();
    }

    @PostMapping("/utilisateurs/search")
    public List<Utilisateur> search(@RequestBody Map<String, String> body){
        String searchTerm = body.get("text");
        return repository.findByFirstnameContaining(searchTerm);
    }

    @PutMapping("/utilisateur/{id}")
    public Utilisateur update(@PathVariable String id, @RequestBody Map<String, String> body){
        int utilisateurId = Integer.parseInt(id);
        // getting blog
        Utilisateur utilisateur = repository.findById(utilisateurId).get();
        utilisateur.setEmail(body.get("email"));
        utilisateur.setPrenom(body.get("prenom"));
        utilisateur.setNom(body.get("nom"));
        return repository.save(utilisateur);
    }

    @DeleteMapping("utilisateur/{id}")
    public boolean delete(@PathVariable String id){
        int questionId = Integer.parseInt(id);
        repository.deleteById(questionId);
        return true;
    }
}
