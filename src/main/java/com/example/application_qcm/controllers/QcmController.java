package com.example.application_qcm.controllers;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.application_qcm.models.Qcm;
import com.example.application_qcm.repositories.QcmRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api")
public class QcmController {

    @Autowired
    private QcmRepository repository;

    @GetMapping("/qcm")
    public List<Qcm> index(){
        return repository.findAll();
    }

    @GetMapping("/qcm/{id}")
    public Qcm show(@PathVariable String id){
        int contactId = Integer.parseInt(id);
        return repository.findById(contactId).get();
    }

    @PostMapping("/qcm/search")
    public List<Qcm> search(@RequestBody Map<String, String> body){
        String searchTerm = body.get("text");
        return repository.findByFirstnameContaining(searchTerm);
    }
    @PostMapping("/qcm")
    public Qcm create(@RequestBody Map<String, String> body){
        String nom = body.get("nom");
        return repository.save(new Qcm(nom));
    }

    @PutMapping("/qcm/{id}")
    public Qcm update(@PathVariable String id, @RequestBody Map<String, String> body){
        int qcmId = Integer.parseInt(id);
        // getting blog
        Qcm qcm = repository.findById(qcmId).get();
        qcm.setNom(body.get("nom"));
        return repository.save(qcm);
    }

    @DeleteMapping("qcm/{id}")
    public boolean delete(@PathVariable String id){
        int qcmId = Integer.parseInt(id);
        repository.deleteById(qcmId);
        return true;
    }
}
