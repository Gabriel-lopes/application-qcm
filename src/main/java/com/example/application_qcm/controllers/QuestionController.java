package com.example.application_qcm.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.application_qcm.models.Question;
import com.example.application_qcm.repositories.QuestionRepository;

@RestController
@RequestMapping("api")
public class QuestionController {
    @Autowired
    private QuestionRepository repository;

    @GetMapping("/questions")
    public List<Question> index(){
        return repository.findAll();
    }

    @GetMapping("/questions/{id}")
    public Question show(@PathVariable String id){
        int questionId = Integer.parseInt(id);
        return repository.findById(questionId).get();
    }

    @PostMapping("/questions/search")
    public List<Question> search(@RequestBody Map<String, String> body){
        String searchTerm = body.get("text");
        return repository.findByFirstnameContaining(searchTerm);
    }

    @PutMapping("/questions/{id}")
    public Question update(@PathVariable String id, @RequestBody Map<String, String> body){
        int questionsId = Integer.parseInt(id);
        // getting blog
        Question question = repository.findById(questionsId).get();
        question.setQuestion(body.get("question"));
        return repository.save(question);
    }

    @DeleteMapping("qcm/{id}")
    public boolean delete(@PathVariable String id){
        int questionId = Integer.parseInt(id);
        repository.deleteById(questionId);
        return true;
    }
}
