package com.example.application_qcm.repositories;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.application_qcm.models.Reponse;

@Repository
public interface ReponseRepository extends JpaRepository<Reponse, Integer> {
    List<Reponse> findByFirstnameContaining(String text);  

}