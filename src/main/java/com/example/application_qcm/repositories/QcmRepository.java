package com.example.application_qcm.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.application_qcm.models.Qcm;

@Repository
public interface QcmRepository extends JpaRepository<Qcm, Integer> {
    List<Qcm> findByFirstnameContaining(String text);  
}
