#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------

DROP DATABASE if exists application_qcm;
CREATE DATABASE application_qcm;
use application_qcm;

#------------------------------------------------------------
# Table: utilisateur
#------------------------------------------------------------

CREATE TABLE utilisateur(
        idUtilisateur Int  Auto_increment  NOT NULL ,
        nom           Varchar (50) NOT NULL ,
        prenom        Varchar (50) NOT NULL ,
        email         Varchar (50) NOT NULL
	,CONSTRAINT utilisateur_PK PRIMARY KEY (idUtilisateur)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: qcm
#------------------------------------------------------------

CREATE TABLE qcm(
        idQcm Int  Auto_increment  NOT NULL ,
        nom   Varchar (50) NOT NULL
	,CONSTRAINT qcm_PK PRIMARY KEY (idQcm)  
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: question
#------------------------------------------------------------

CREATE TABLE question(
        idQuestion Int  Auto_increment  NOT NULL ,
        question   Varchar (255) NOT NULL ,
        idQcm      Int NOT NULL
	,CONSTRAINT question_PK PRIMARY KEY (idQuestion)

	,CONSTRAINT question_qcm_FK FOREIGN KEY (idQcm) REFERENCES qcm(idQcm)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: reponse
#------------------------------------------------------------

CREATE TABLE reponse(
        idReponse  Int  Auto_increment  NOT NULL ,
        reponse    Varchar (255) NOT NULL ,
        veracite   Bool NOT NULL ,
        idQuestion Int NOT NULL
	,CONSTRAINT reponse_PK PRIMARY KEY (idReponse)

	,CONSTRAINT reponse_question_FK FOREIGN KEY (idQuestion) REFERENCES question(idQuestion)
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: score
#------------------------------------------------------------

CREATE TABLE score(
        idQcm         Int NOT NULL ,
        idUtilisateur Int NOT NULL ,
        score         Int NOT NULL
	,CONSTRAINT score_PK PRIMARY KEY (idQcm,idUtilisateur)

	,CONSTRAINT score_qcm_FK FOREIGN KEY (idQcm) REFERENCES qcm(idQcm)
	,CONSTRAINT score_utilisateur0_FK FOREIGN KEY (idUtilisateur) REFERENCES utilisateur(idUtilisateur)
)ENGINE=InnoDB;




#----------------------------------------------------------***
# Données #
#----------------------------------------------------------***

INSERT INTO utilisateur (nom, prenom, email) VALUES 
('Correia Lopes', 'Gabriel', 'g@gmail.com'),
('Ismail', 'Mustafa', 'm@gmail.com'),
('Benachir', 'Hamza', 'h@gmail.com');


INSERT INTO qcm (nom) VALUES 
('Coupe du monde 2022'),
('Geographie'),
('Histoire');


INSERT INTO question (question, idQcm) VALUES 
('Qui a gagné la coupe du monde 2022 ?', 1),
('Quels est/sont le/les pays en frontière(s) du Mexique ?', 2),
('En quelle année Clovis I a t-il été sacré ?', 3);

INSERT INTO reponse (reponse, veracite, idQuestion) VALUES 
('France', FALSE, 1),
('Maroc', FALSE, 1),
('Argentine', TRUE, 1),
('Croatie', FALSE, 1),

('USA', TRUE, 2),
('Belise', TRUE, 2),
('Guatemala', TRUE, 2),
('Brésil', FALSE, 2),

('581', FALSE, 3),
('481', TRUE, 3);